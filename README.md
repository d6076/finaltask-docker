# Для запуска приложения необходимо выполнить start.sh

`./start.sh`

# Если что-то пошло не так:
1. Проверить создана ли сеть internal_network
`docker network ls | grep internal_network`

2. Развернуть стэк EFK
`docker-compose -p rbmdkrfinalefk -f efk.compose.yml up -d`

3. Развернуть стэк с приложением
`docker-compose -p rbmdkrfinalapp -f app.compose.yml up -d`

При первом запуске для инициализации базы данных необходимо раскоментировать 73 строку в app.compose.yml,
после инициализации нужно закоментировать обратно, иначе при каждом перезапуске стэка, база будет обнулятся.
`# - ${PS_DATA_PATH_LOCAL}/dbinit:/docker-entrypoint-initdb.d`

Для работы с базой можно использовать pgadmin
`docker-compose -p pgadmin -f pgadmin.yml up -d`

![LibreSpeed Logo](https://github.com/librespeed/speedtest/blob/master/.logo/logo3.png?raw=true)

# LibreSpeed - Docker Version

This is the Docker version of LibreSpeed.

See the included `doc.md` or the wiki for instructions on how to use it.

## Donate
[![Donate with Liberapay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/fdossena/donate)  
[Donate with PayPal](https://www.paypal.me/sineisochronic)  

## License
Copyright (C) 2016-2019 Federico Dossena

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/lgpl>.
