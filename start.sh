#!/bin/bash

NET_EXIST=`docker network ls | grep internal_network`
echo $NET_EXIST

if [[ -n $NET_EXIST ]]
then
echo "internal_network is exist"
else
echo "internal_network is not  exist"
docker network create internal_network
fi

sleep 5
docker-compose -p rbmdkrfinalefk -f efk.compose.yml up -d
sleep 5
docker-compose -p rbmdkrfinalapp -f app.compose.yml up -d
sleep 2

echo "APP - http://speedtest.51.250.20.187.nip.io/"
echo "KIBANA - http://kibana.51.250.20.187.nip.io/"
echo "TRAEFIK - http://traefik.51.250.20.187.nip.io/"